/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('chatbox', require('./components/Chatbox.vue'));
Vue.component('chat', require('./components/Chat.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('onlineuser', require('./components/OnlineUser.vue'));
Vue.component('sidebar-ele', require('./components/SidebarElement.vue'));


console.log(require('./components/OnlineUser.vue'));
/**
 * Push notification onto web browser
 */
function browserNotification(message){
  Notification.requestPermission( permission => {
    let notification = new Notification('Chat Laravel', {
      body: message, // content for the alert
      icon: "https://pusher.com/static_logos/320x320.png" // optional image url
    });
    // link to page on clicking the notification
    notification.onclick = () => {
      window.open(window.location.href);
    };
  });
}


// export default {
//
//     // The install method will be called with the Vue constructor as the first argument, along with possible options
//     install (Vue, options) {
//       // Add $plugin instance method directly to Vue components
//       Vue.prototype.$onlineUsers=''
//     }
//   }

const app = new Vue({
    el: '#app',
    data: {
        onlineUsers:'',
    },
    methods:{
      // Add new chat box without reloading the whole page
      addChatBox: function(userId,otherId){
              if (_.find(this.onlineUsers, {id: otherId})) {
              // if (1==1){
                if ($('#chatbox'+ otherId).length==0){
                  //The chat box does not exist, create an mount in the DOM for the first and only time
                   $("#chat-footerbar").append('<div class="chatbox p-2 bd-highlight" id="chatbox'+ otherId+'"></div>');
                   $('#chatbox'+ otherId).append('<div id="chatbox'+ otherId+'bis"></div>');
                   axios.post('/chat/getChat/' + otherId).then((response) => {
                        console.log(response.data);
                        // chats = response.data;
                        var ChatboxComponent = Vue.extend({
                          template: '<chatbox v-bind:chats="chats" v-bind:userid="userid" v-bind:otherid="otherid" v-bind:othername="othername"></chatbox>',
                          data: function () {
                            return {
                              chats: response.data,
                              userid: userId,
                              otherid: otherId,
                              othername:$('meta[name="user'+otherId+'"]').attr('content')
                            }
                          },
                          mounted(){
                            //Scroll chat history to bottom
                            $('#chat-scroll'+otherId).animate({
                                  scrollTop: $('#chat-scroll'+otherId).get(0).scrollHeight
                            }, 2000);
                            //Active toggle down button
                            $('#chatbox'+otherId).find(".btnToggle").click(function() {
                                console.log($('#chatbox'+otherId).find(".msg_history"));
                                $('#chatbox'+otherId).find(".msg_history").toggleClass("chatboxToggle");
                                $('#chatbox'+otherId).find(".type_msg").toggleClass("chatboxHidden");
                                $('#chatbox'+otherId).toggleClass("chatboxToggle");
                                if($('#chatbox'+otherId).find(".btnToggle").find("i").hasClass("fa-arrow-down")){
                                  $('#chatbox'+otherId).find(".btnToggle").find("i").toggleClass("fa-arrow-down").toggleClass("fa-arrow-up");
                                }else{
                                  $('#chatbox'+otherId).find(".btnToggle").find("i").toggleClass("fa-arrow-up").toggleClass("fa-arrow-down");
                                }
                            });
                            $('#chatbox'+otherId).find(".btnClose").click(function() {
                                $('#chatbox'+otherId).toggleClass("chatboxHidden");
                            });
                            //Retrieve chat'history for rendering Vue component
                            // Listen for new message and then update data
                                Echo.private('Chat.'+ userId)
                                    .listen('BroadcastChat', (e) => {
                                      if (otherId == e.sender.id){
                                        document.getElementById('ChatAudio').play();
                                        this.chats.push(e.chat);
                                        $('#chat-scroll'+otherId).animate({
                                              scrollTop: $('#chat-scroll'+otherId).get(0).scrollHeight
                                        }, 2000);
                                        browserNotification(e.sender.name+': '+e.chat.chat);
                                      }
                                });
                          }
                        });
                        new ChatboxComponent().$mount('#chatbox'+ otherId+'bis');
                   });
                 }else{
                //The chat box is already created, just show it if it's hidden
                    $('#chatbox'+otherId).toggleClass("chatboxHidden");
                 }
              }else{
                  //User is not online
                  // TODO
                  alert('Wait until '+$('meta[name="user'+otherId+'"]').attr('content')+' is online to start the conversation');
              }

      },
      // Manipulation of Chatbox's position
      toggleChatbox: function(id){
        $("#chatbox"+id).toggleClass("chatbox-down");
      }
    },
    created() {
        const userId = $('meta[name="userId"]').attr('content');
        // Listen to everyone
        Echo.private('Chat.'+userId)
            .listen('BroadcastChat', (e) => {
              if ($('#chatbox'+e.sender.id).length==0){
                document.getElementById('ChatAudio').play();
                this.addChatBox(userId,e.sender.id);
                browserNotification(e.sender.name+': '+e.chat.chat);
              }
            });
        //Join the broadcasting channel for online status verification
        //Hanlde different events
        if (userId != 'null') {
            Echo.join('Online')
                .here((users) => {
                    this.$root.onlineUsers = users;
                })
                .joining((user) => {
                    if ($('meta[name="user'+user.id+'"]').length==1){
                      this.$root.onlineUsers.push(user);
                      browserNotification(user.name+' is now online!');
                    }else{
                      //TODO setup update process for status of new added user
                      // for the moment, force to reload the webpage
                      window.location.reload(true);
                      // console.log(this.$root.onlineUsers);
                      // this.$root.onlineUsers.push(user);
                      // var SidebarComponent = Vue.extend({
                      //   template: '<sidebar-ele v-bind:userid="userid" v-bind:newuser="newuser" v-bind:onlineusers="onlineusers"></sidebar-ele>',
                      //   data: function () {
                      //     return {
                      //       userid: userId,
                      //       newuser: user,
                      //       onlineusers:this.$root.onlineUsers
                      //     }
                      //   }
                      // });
                      $('#listUserOnSidebar').append('<div id="addedElement"></div>');
                      new SidebarComponent().$mount('#addedElement');
                      browserNotification(user.name+' has just connected to the network!');
                    }

                })
                .leaving((user) => {
                    this.$root.onlineUsers = this.$root.onlineUsers.filter(u => u.id != user.id);
                    browserNotification(user.name+' is now offline!');
                });
        }
    }
});
