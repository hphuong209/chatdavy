<button id="toggleSideBarBtn" onclick="toggleWrapper()" type="button"><i class="fa fa-arrow-to-bottom"></i></button>
<div id="listUserOnSidebar" style="overflow:auto;height:100%">
  <audio id="ChatAudio">
      <source src="{{ asset('sounds/chatsound.mp3') }}">
  </audio>
    @foreach (App\User::all() as $user)
      @if ($user->id != Auth::id())
            <meta name="user{{ $user->id }}" content="{{ $user->name }}"></meta>
            <div v-on:click="addChatBox({{ Auth::id() }},{{ $user->id }})" class="chat_list">
              <div class="chat_people">
                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                <div class="chat_ib">
                  <!-- <div v-on:click="testtest()"  class="d-flex justify-content-between align-items-center w-100"> -->
                    <div  class="d-flex justify-content-between align-items-center w-100">
                      <h5>{{ $user->name }} </h5>
                      <onlineuser v-bind:user="{{ $user }}" v-bind:onlineusers="onlineUsers"></onlineuser>
                    </div>
                </div>
              </div>
            </div>
      @endif
  @endforeach
</div>
