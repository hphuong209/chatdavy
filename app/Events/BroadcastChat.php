<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Chat;
use App\User;
class BroadcastChat implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat;
    public $sender;
    /**
     * Create a new event instance.
     *
     * @return void
     */
     public function __construct(Chat $chat,User  $sender)
     {
       $this->chat = $chat;
       $this->sender = $sender;
     }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('Chat.'. $this->chat->other_id);
        // return new PrivateChannel('Chat.' . $this->chat->user_id . '.' . $this->chat->other_id);

    }
}
