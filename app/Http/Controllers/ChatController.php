<?php

namespace App\Http\Controllers;
use App\Events\BroadcastChat;
use Illuminate\Http\Request;
use Auth;
use App\Chat;

class ChatController extends Controller
{
      /**
       * Retrieve messages from database
       *
       * @return \Illuminate\Http\Response
       */
      public function getChat($id) {
         $chats = Chat::where(function ($query) use ($id) {
             $query->where('user_id', '=', Auth::user()->id)->where('other_id', '=', $id);
         })->orWhere(function ($query) use ($id) {
             $query->where('user_id', '=', $id)->where('other_id', '=', Auth::user()->id);
         })->get();
         return $chats;
       }

       /**
        * Add messages into database and trigger broadcast event
        *
        * @return \Illuminate\Http\Response
        */
       public function sendChat(Request $request) {
           $chat = Chat::create([
               'user_id' => $request->user_id,
               'other_id' => $request->other_id,
               'chat' => $request->chat
           ]);
          event(new BroadcastChat($chat,Auth::user()));
          return [];
       }
}
