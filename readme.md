## About 

Chat application web based on the Laravel framework

## Installation

I have deployed the application on the apache2 serveur using PHP 7.1. 

[PUSHER](https://laravel.com/docs/5.7/broadcasting)'s PHP driver is required for the Broadcasting service, for its installation:
```sh
$ composer require pusher/pusher-php-server "~3.0"
```
Clone this repository on your server, then install JavaScript's dependencies:
```sh
$ cd /path/to/project/root
$ npm install
$ npm run wacth
```
Different JavaScript packages have been used, see `package.json` file


- [Font Awesome](https://fontawesome.com/): for icon rendering
- [laravel-echo & pusher-js](https://laravel.com/docs/5.7/broadcasting): for client-side broadcasting service


Configure the `.env` file of the project by providing your Database's information (I used MysSQL server). To initialize the databases, run:
```sh
$ cd /path/to/project/root
$ php artisan migrate
```



