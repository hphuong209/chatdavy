<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('Chat.{user_id}.{other_id}', function ($user, $user_id, $other_id) {
    return $user->id == $other_id;
});

Broadcast::channel('Chat.{other_id}', function ($user, $other_id) {
    return $user->id == $other_id;
});


Broadcast::channel('Online', function ($user) {
    return $user;
});
